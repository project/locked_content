<?php

/**
 * @file
 * Views integration functions for the locked_content module.
 */

/**
 * Implements hook_views_data().
 */
function locked_content_views_data() {
  $data = array();

  $data['locked_content']['table']['group'] = t('Content');

  $data['locked_content']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'locked_content' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['locked_content']['locked_content'] = array(
    'title' => t('Locked'),
    'help' => t('Whether or not the node is locked.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'locked_content_handler_filter_locked',
      'label' => t('Locked'),
      'type' => 'yes-no',
    ),
  );

  return $data;

}
