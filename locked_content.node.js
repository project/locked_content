(function ($) {

Drupal.behaviors.lockedFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.node-form-locked', context).drupalSetSummary(function (context) {
      var checkbox = $('.form-item-locked input', context);
      if (checkbox.is(':checked')) {
        return Drupal.t('Locked');
      }

      return Drupal.t('Unlocked');
    });
  }
};

})(jQuery);
