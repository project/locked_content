Locked Content satisfies a specific access control use case: you want
users to be able to see teasers, links, and search results for locked
materials, but want them to log in before actually visiting the full page
view.
It's useful for situations where keeping the existence of information
secret isn't critical, but making sure users are logged in before they
actually view the complete contents is important.

Locking is available for all content by default, but can be turned off,
switch to "on by default," or switch to "always locked" on a content-type-
by-content-type basis.
It also walks up the menu tree to see if any parents are locked, and
treats any children as locked.

Locked Content was originally created for the Wisconsin South Central
Library System by Lullabot. Librarians rock.
