<?php

/**
 * @file
 * Views handlers for the locked_content module.
 */

/**
 * Class locked_content_handler_filter_locked.
 */
class locked_content_handler_filter_locked extends views_handler_filter_boolean_operator {

  /**
   * Public function construct.
   */
  public function construct() {
    parent::construct();
    $this->value_value = t('Locked');
  }

  /**
   * Public function query.
   */
  public function query() {
    $this->ensure_my_table();
    $qualified_name = "$this->table_alias.$this->real_field";
    $this->query->add_where_expression($this->options['group'], $qualified_name . (empty($this->value) ? " = 0 OR $qualified_name IS NULL" : ' = 1'));
  }

}
